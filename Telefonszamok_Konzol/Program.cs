﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telefonszamok_DAL_Konzol;
using Telefonszamok_DAL_Konzol;

namespace Telefonszamok_Konzol
{
    class Program
    {
        static cnTelefonszamok cnTelefonszamok;
        static void Main(string[] args)
        {
            cnTelefonszamok = new cnTelefonszamok();
            // Adatfelvitel();
            Lekerdez();
        }

        private static void Adatfelvitel()
        {
            var helyseg = new KaveNevek { KavePont = 2000, KaveNev = "Teszthelyseg" };
            var szemely = new Szemely
            {
                Vezeteknev = "",
                Utonev = "",
                Lakcim = "",
                enHelyseg = helyseg
            };

            helyseg.enSzemelyek.Add(szemely);
            var t1 = new KavePontX { KavepontSzorzo = "x 1", enSzemely = szemely };
            var t2 = new KavePontX { KavepontSzorzo = "x 1", enSzemely = szemely };
            szemely.enTelefonszamok.Add(t1);
            szemely.enTelefonszamok.Add(t2);
            cnTelefonszamok.enHelysegek.Add(helyseg);
            cnTelefonszamok.enSzemelyek.Add(szemely);
            cnTelefonszamok.enTelefonszamok.Add(t1);
            cnTelefonszamok.enTelefonszamok.Add(t2);
            cnTelefonszamok.SaveChanges();
        }

        private static void Lekerdez()
        {
            Console.WriteLine("Összes adat\r\n-------");
            foreach (var x in cnTelefonszamok.enSzemelyek)
            {
                var s = x.Vezeteknev + " " + x.Utonev + " " +
                    x.enHelyseg.KavePont + " " + x.enHelyseg.KaveNev + " " +
                    x.Lakcim + ", ";
                foreach (var y in x.enTelefonszamok)
                {
                    s += y.KavepontSzorzo;
                    if (y != x.enTelefonszamok.Last()) s += ", ";
                }
            Console.WriteLine(s);
            Console.ReadLine();
            }
        }

    }
}
