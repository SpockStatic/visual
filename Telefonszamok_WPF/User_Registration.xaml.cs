﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SqlClient;

namespace User_Registration
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string connectionString = @"Data Source = (local)\sql; Initial Catalog = UserRegistrationDB; Integrated Security=True;";

        public MainWindow()
        {
            InitializeComponent();
        }

        private void BtmSubmit_Click(object sender, RoutedEventArgs e)
        {

            if (txtUsername.Text == "" || txtPassword.Text == "")
                MessageBox.Show("Tőltse ki a kért mezőket");
            else if (txtPassword.Text != txtConfirmPassword.Text)
                MessageBox.Show("Nem eggyeznek a jelszavak");
            else
            {
                /*
                using (SqlConnection sqlCon = new SqlConnection(connectionString))
                {

                    sqlCon.Open();
                    SqlCommand sqlCmd = new SqlCommand("UserAdd", sqlCon);
                    sqlCmd.CommandType = System.Data.CommandType.StoredProcedure;
                    sqlCmd.Parameters.AddWithValue("@FirstName", txtFirstName.Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@LastName", txtLastName.Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@Username", txtUsername.Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@Password", txtPassword.Text.Trim());
                    sqlCmd.ExecuteNonQuery(); */

                    Telefonszamok_WPF.MainWindow x = new Telefonszamok_WPF.MainWindow();
                    x.Show();

                    /* Clear(); */

                }

            }

        }

       /* void Clear()
        {

            txtFirstName.Text = txtLastName.Text = txtUsername.Text = txtPassword.Text = " ";

        }

    } */
}

